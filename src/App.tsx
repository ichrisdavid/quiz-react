import React, { useState} from 'react';
import QuestionCard from './components/QuestionCard';
import {fecthQuizQuestions} from './API';
import {Difficulty, QuestionState} from './API'
import { nextTick } from 'process';
//styles

import { GlobalStilos, Wrapper  } from './App.styles';

export type AnwserObject = {
  question: string;
  answer:string;
  correct:boolean;
  correctAnswer:string;

}

const TOTAL_QUESTION = 10;

const App = () => {
  const [loading, setLoading] = useState(false);
  const [questions,setQuestions] = useState<QuestionState[]>([]);
  const [number,setNumber] = useState(0);
  const [userAnswers,setUserAnswers] = useState<AnwserObject[]>([]);
  const [score, setScore] = useState(0)
  const [gameOver, setGameOver] =useState(true);


console.log(questions)

  const startTrivia = async () => {
    setLoading(true);
    setGameOver(false);

    
    const newQuestions = await fecthQuizQuestions(
      TOTAL_QUESTION,
      Difficulty.EASY
    );

    setQuestions(newQuestions);
    setScore(0);
    setUserAnswers([])
    setNumber(0)
    setLoading(false);
  }

  const checkAnswer = (e:React.MouseEvent<HTMLButtonElement>) =>{
    //Respuesta del usuario
    if(!gameOver){
      const answer = e.currentTarget.value;
      //se verifica la respuesta dle usuario con el valor correcto de la pregunta
      const correct = questions[number].correct_answer === answer;
      if(correct) setScore(prev => prev + 1);
      //se guarda la respuesta en el array de respuestas de usuarios
      const anwserObject = {
        question: questions[number].question,
        answer:answer,
        correct:correct,
        correctAnswer:questions[number].correct_answer
      };

      setUserAnswers(prev => [...prev,anwserObject]);

    }
  }

  const nextQuestion = () =>{
    //se mueve a la siguiente pregunta si no es la ultima

    const nextQuestion = number + 1;

    if(nextQuestion === TOTAL_QUESTION){
      setGameOver(true)
    }else{
      setNumber(nextQuestion);
    }

  }

  return (
    <>
    <GlobalStilos/>
  <Wrapper>
    <h1>Quiz React Typescript</h1>
    {gameOver || userAnswers.length === TOTAL_QUESTION?(
      <button className="start" onClick={startTrivia}>
      Start
    </button>
    ):null}
    {!gameOver ? <p className="score">Score:{score}</p>:null }
    {loading && <p>Loading Question ...</p>}
    {!loading && !gameOver && (
    <QuestionCard
    quiestionNR = {number + 1}
    totalQuestion = {TOTAL_QUESTION}
    question = {questions[number].question}
    answers = {questions[number].answers}
    userAnswer= {userAnswers?userAnswers[number]:undefined}
    callback = {checkAnswer}

    />
    )}
    {!gameOver && 
    !loading && 
    userAnswers.length === number + 1 
    && number != TOTAL_QUESTION - 1?(
      <button className="next" onClick={nextQuestion}>
      Next Question
      </button>
    ):null}
    
  </Wrapper>
  </>
  );

}

export default App;
